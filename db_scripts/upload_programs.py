print('to run: ./manage.py shell < upload_csv.py')
import secrets
from backend.models import Program,Manager
import csv
import os

path =  './db_scripts/programs.csv'
l = [2,5,10,11,12,13,14]
f = open(path)
manager = Manager.objects.all()[0]
with open(path) as f:
    reader = csv.reader(f)
    next(reader)
    #length = len(list(reader))
    for row_number,row in enumerate(reader):
        print(row_number)
        _,created = Program.objects.get_or_create(posted_on=row[5],program_perks=row[6],program_duration="12:21:00",application_deadline=row[7],company_name=row[2],program_name=row[1],program_summary=row[3],company_website=row[4],posted_by=Manager.objects.get(pk=secrets.choice(l)))