from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone

# Create your models here.


class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)

    class Meta(object):
        unique_together = ('email',)


class Manager(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    phone = models.CharField(unique=True, max_length=12)
    company = models.CharField(max_length=255)

    def __str__(self):
        return self.user.username


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


STATUS_CHOICES = (
    ("pending", "Pending"),
    ("accepted", "Accepted"),
    ("rejected", "Rejected"),
)


class Program(models.Model):
    company_name = models.CharField(max_length=255, blank=False)
    company_logo = models.ImageField(upload_to='media/')
    company_website = models.URLField(max_length=2000)
    program_name = models.CharField(max_length=255, blank=False)
    program_summary = models.TextField(blank=False)
    application_deadline = models.DateTimeField(blank=True)
    program_duration = models.DurationField()
    program_perks = models.TextField()
    program_category = models.ManyToManyField(
        Category, blank=True, related_name='tagged_programs')
    posted_by = models.ForeignKey(
        Manager, on_delete=models.CASCADE, related_name='programs_posted')
    posted_on = models.DateTimeField(default=timezone.now)
    status = models.CharField(
        max_length=15, choices=STATUS_CHOICES, default="pending")

    @property
    def applicants_count(self):
        return self.taken_program.count()

    # @property
    # def applicants(self):
    #     return Student.objects.filter(taken_program__program=self)

    def __str__(self):
        return f"{self.company_name}: {self.program_name}"


class Question(models.Model):
    program = models.ForeignKey(
        Program, on_delete=models.CASCADE, related_name='questions')
    text = models.CharField(max_length=500)

    def __str__(self):
        return self.text


class Country(models.Model):
    name = models.CharField(max_length=255)
    country_code = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class State(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


# class City(models.Model):
#     state = models.ForeignKey(State, on_delete=models.CASCADE)
#     name = models.CharField(max_length=30)

#     def __str__(self):
#         return self.name

class District(models.Model):
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Academy_Stream(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class College(models.Model):
    university_name = models.CharField(max_length=255)
    college_name = models.CharField(max_length=255)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    college_type = models.CharField(max_length=255)
    no_of_students = models.IntegerField(default=0)
    description = models.TextField(blank=True)
    website = models.URLField(blank=True, default='')
    email_id = models.EmailField(blank=True, default='')
    phone = models.CharField(default='', blank=True, max_length=12)

    @property
    def country(self):
        return self.state.country.name

    def __str__(self):
        return self.college_name


class Student(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    age = models.IntegerField(default=0)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female')
    )
    gender = models.CharField(choices=GENDER_CHOICES, max_length=128)
    college = models.ForeignKey(College, on_delete=models.CASCADE)
    academic_streams = models.ForeignKey(
        Academy_Stream, on_delete=models.CASCADE, default=None, blank=True, null=True)
    facebook_url = models.URLField(blank=True)
    twitter_url = models.URLField(blank=True)
    insta_url = models.URLField(blank=True)
    blog = models.URLField(blank=True)
    about_myself = models.TextField(blank=True)
    programs = models.ManyToManyField(Program, through='TakenProgram')

    def __str__(self):
        return self.user.first_name


class TakenProgram(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='taken_program')
    program = models.ForeignKey(
        Program, on_delete=models.CASCADE, related_name='taken_program')
    score = models.FloatField(default=0)
    date = models.DateTimeField(auto_now_add=True)

    @property
    def answers(self):
        return self.student.program_answers.filter(question__program=self.program)

    def __str__(self):
        return self.student.user.first_name + ":" + self.program.program_name

    class Meta:
        unique_together = ('student', 'program',)


class Answer(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='program_answers')
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name='answers')
    text = models.CharField(max_length=1000)

    def __str__(self):
        return self.text

    class Meta:
        unique_together = ('student', 'question',)

# class StudentAnswer(models.Model):
#     student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='program_answers')
#     answer = models.ForeignKey()
