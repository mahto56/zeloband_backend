from rest_framework import permissions


class IsStudent(permissions.BasePermission):
    message = 'Only students allowed.'

    def has_permission(self, request, view):
        return request.user.is_student


class IsManager(permissions.BasePermission):
    message = 'Only managers allowed.'

    def has_permission(self, request, view):
        return request.user.is_manager
