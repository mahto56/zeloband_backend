print('to run: ./manage.py shell < upload_csv.py')
from backend.models import State,District,Country,College
import os,csv

path =  './db_scripts/college.csv'

f = open(path)
length = len(f.readlines())
india = Country.objects.get(name='India')
with open(path) as f:
    reader = csv.reader(f)
    next(reader)
    #length = len(list(reader))
    for row_number,row in enumerate(reader):
        
        #print(row[3].capitalize())
        state,created = State.objects.get_or_create(country=india,name=row[4].capitalize())
        district,created = District.objects.get_or_create(state=state,name=row[5].capitalize())  
        university = row[1].split('(')[0]
        college = row[2].split('(')[0]
        college_type = row[3].capitalize()
        _,created = College.objects.get_or_create(university_name=university,college_name=college,state=state,district=district,college_type=college_type)
        # print(university)
        if row_number==100:
            break
        read_percentage = round(100*(row_number/length),1)
        print(f'{read_percentage}% inserted',end="\r")
print()
print("done!")