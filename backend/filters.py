from django_filters import rest_framework as filters
from .models import *


class ProgramFilter(filters.FilterSet):
    program_name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Program
        fields = ('posted_by',)


class StudentFilter(filters.FilterSet):

    class Meta:
        model = Student
        fields = ('college',)

#not working


class StateFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Model:
        model = State
        fields = ('id', 'name',)


class CollegeFilter(filters.FilterSet):
    university_name = filters.CharFilter(lookup_expr='icontains')
    college_name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = College
        fields = ('state', 'district', 'university_name',
                  'college_type', 'college_name')


class TakenProgramFilter(filters.FilterSet):

    class Model:
        model = TakenProgram
        fields = ('program',)
