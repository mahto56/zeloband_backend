from rest_framework import serializers
from .models import *
from notifications.models import *

from django.contrib.auth import get_user_model
User = get_user_model()


class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('password', 'last_login',
                   'is_staff', 'groups', 'user_permissions'
                   )


class NotificationSerializer(serializers.ModelSerializer):
    # recipient = UserSerializer(read_only=True)
    # verb = serializers.CharField(read_only=True)
    # unread = serializers.BooleanField(read_only=True)

    class Meta:
        model = Notification
        fields = ('recipient', 'verb', 'unread', 'timestamp')


class ManagerSerializer(serializers.ModelSerializer):
    # add uniqueness
    programs_posted = 'ProgramsSerializer(read_only=True, many=True)'
    user = UserSerializer(read_only=True)

    class Meta:
        model = Manager
        fields = '__all__'

    def update(self, instance, validated_data):
        'to be done'
        return instance


class StudentSerializer(serializers.ModelSerializer):
    # add uniqueness
    # programs = ProgramsSerializer(many=True)
    # user = UserSerializer(read_only = True)

    class Meta:
        model = Student
        fields = '__all__'

    def update(self, instance, validated_data):
        'to be done'
        return instance


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        exclude = ('program',)


class AnswerSerializer(serializers.ModelSerializer):
    question = QuestionSerializer()

    class Meta:
        model = Answer
        exclude = ('student',)

    def update(self, instance, validated_data):
        'to be done'
        return instance


class ProgramsSerializer(serializers.ModelSerializer):
    #posted_by = ManagerSerializer(read_only=True)
    posted_by = serializers.CharField(source="posted_by.user.username")
    applicants_count = serializers.ReadOnlyField()
    # applicants = StudentSerializer(many=True)

    class Meta:
        model = Program
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.company_name = validated_data.get(
            "company_name", instance.company_name)
        instance.company_logo = validated_data.get(
            "company_logo", instance.company_logo)
        instance.company_website = validated_data.get(
            "company_website", instance.company_logo)
        instance.program_name = validated_data.get(
            "program_name", instance.program_name)
        instance.program_summary = validated_data.get(
            "program_summary", instance.program_summary)
        instance.application_deadline = validated_data.get(
            "application_deadline", instance.application_deadline)
        instance.program_duration = validated_data.get(
            "program_duration", instance.program_duration)
        instance.program_perks = validated_data.get(
            "program_perks", instance.program_perks)
        instance.save()
        return instance


class TakenProgramSerializer(serializers.ModelSerializer):
    # student = StudentSerializer(read_only = True)
    answers = AnswerSerializer(many=True)
    program = ProgramsSerializer()

    class Meta:
        model = TakenProgram
        fields = '__all__'
    # def update(self,instance,validated_data):
    #     instance.company_name = validated_data.get("company_name",instance.company_name)
    #     instance.company_logo = validated_data.get("company_logo",instance.company_logo)
    #     instance.company_website = validated_data.get("company_website",instance.company_logo)
    #     instance.program_name = validated_data.get("program_name",instance.program_name)
    #     instance.program_summary = validated_data.get("program_summary",instance.program_summary)
    #     instance.application_deadline = validated_data.get("application_deadline",instance.application_deadline)
    #     instance.program_duration = validated_data.get("program_duration",instance.program_duration)
    #     instance.program_perks = validated_data.get("program_perks",instance.program_perks)
    #     instance.save()
    #     return instance


class DistrictSerializer(serializers.ModelSerializer):

    class Meta:
        model = District
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = '__all__'


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'


class CollegeSerializer(serializers.ModelSerializer):
    # state = serializers.CharField(source='state.name')
    #state = serializers.PrimarKeyRelatedField(queryset=State.objects.all())

    class Meta:
        model = College
        fields = '__all__'
