from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from notifications.signals import notify
from .models import Program

User = get_user_model()

admin = User.objects.filter(is_superuser=True)


@receiver(post_save, sender=Program)
def handler(sender, instance, created, **kwargs):
    notify.send(instance, recipient=admin,
                verb=f'was added/saved of user {instance.posted_by}')
