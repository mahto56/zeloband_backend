from django.urls import path
from .views import *

urlpatterns = [
    path('programs/', ListCreateProgramsView.as_view(), name='programs_all'),
    path('programs/<int:pk>/', ProgramDetailView.as_view(), name='single_program'),
    path('applications/', ListCreateTakenProgramView.as_view(),
         name='taken_programs_all'),
    path('applications/<int:pk>', TakenProgramDetailView.as_view(),
         name='taken_programs_single'),

    path('applied/', ListCreateAppliedProgramView.as_view(),
         name='applied_programs_all'),

    path('notifications/', ListNotificationView.as_view(),
         name='notification_all'),

    path('student/', ListStudentView.as_view(), name='students_all'),
    path('user/', ListUserView.as_view(), name='users_all'),
    path('district/', ListDistrictView.as_view(), name='all_district'),
    path('state/', ListStateView.as_view(), name='all_state'),
    path('category/', ListCategoryView.as_view(), name='all_category'),
    path('country/', ListCountryView.as_view(), name='all_country'),
    path('college/', ListCollegeView.as_view(), name='all_college'),
    path('college/<int:pk>/', CollegeDetailView.as_view(), name='single_college'),
    path('auth/login/', LoginView.as_view(), name='login'),
    path('auth/register/manager/',
         RegisterManagerView.as_view(), name='manager_register'),
    path('auth/register/student/',
         RegisterStudentView.as_view(), name='student_register'),
    path('auth/verify/', verify_email.as_view(), name="verify_email"),
    path('auth/verify/<uidb64>/<token>/', activate, name="activate"),
    path('auth/clear_session/', clearSession, name="clear-session")
]
