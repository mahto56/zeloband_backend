print('to run: ./manage.py shell < upload_csv.py')
from backend.models import Category
import csv


path =  './db_scripts/categories.csv'

with open(path) as f:
    reader = csv.reader(f)
    #length = len(list(reader))
    for row_number,row in enumerate(reader):
        print(row_number)
        _,created = Category.objects.get_or_create(name=row[0].replace('.',''))