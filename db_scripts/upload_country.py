print('to run: ./manage.py shell < upload_csv.py')
from backend.models import Country
import csv
import os

path =  './db_scripts/country.csv'

f = open(path)
# f2 = open('./db_scripts/country_mongo.csv',"w")
# f2.write("name,country_code\n")
length = len(f.readlines())
with open(path) as f:
    reader = csv.reader(f)
    next(reader)
    #length = len(list(reader))
    for row_number,row in enumerate(reader):
        _,created = Country.objects.get_or_create(name=row[0],country_code=row[1])
        # f2.write(f"{row[0]},{row[1]}\n")
        read_percentage = round(100*(row_number/length),1)
        print(f'{read_percentage}% inserted',end="\r")
# f2.close()