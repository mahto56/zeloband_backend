print('to run: ./manage.py shell < upload_csv.py')
from backend.models import State,Country
import csv
import os

path =  './db_scripts/states.csv'

f = open(path)
length = len(f.readlines())
india = Country.objects.get(name="India")
with open(path) as f:
    reader = csv.reader(f)
    #next(reader)
    #length = len(list(reader))
    for row_number,row in enumerate(reader):
        #print(row[1].capitalize())
        _,created = State.objects.get_or_create(country=india,name=row[1].capitalize())
        read_percentage = round(100*(row_number/length),1)
        print(f'{read_percentage}% inserted',end="\r")