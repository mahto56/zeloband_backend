from rest_framework import generics
from .models import Student, Program, District, Manager
from .serializers import *
from .pagination import *
from django.db import IntegrityError
from django.shortcuts import redirect

from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework.views import status
# from .decorators import validate_request_data


from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from rest_framework_jwt.settings import api_settings
from rest_framework import permissions

from .filters import *

# import djongo
from django.db.models import Prefetch

# for email
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string, get_template
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.template import Context
from rest_framework.decorators import api_view
from notifications.models import *


# Get the JWT settings, add these lines after the import/from lines
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()
# from .auth import IsStudent
from .permission import IsStudent,IsManager


class ListUserView(generics.ListAPIView):
    """
    GET students/
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    #filterset_class = StudentFilter


class ListStudentView(generics.ListAPIView):
    """
    GET students/
    """
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = StudentFilter


class ListCreateProgramsView(generics.ListCreateAPIView):
    """
    GET program/
    POST programs/
    """
    def get_queryset(self):
        student = not self.request.user.is_anonymous and self.request.user.is_student
        if student:
            return Program.objects.prefetch_related(
                Prefetch('program_category', queryset=Category.objects.only('pk').all()),
                Prefetch('posted_by', queryset=Manager.objects.only('pk').all())
            ).order_by('id').filter(taken_program__student=self.request.user.student)
        return Program.objects.prefetch_related(
        Prefetch('program_category', queryset=Category.objects.only('pk').all()),
        Prefetch('posted_by', queryset=Manager.objects.only('pk').all())
    ).order_by('id')

    serializer_class = ProgramsSerializer
    # permission_classes = (permissions.IsAuthenticated,)
    filterset_class = ProgramFilter
    pagination_class = ProgramPagination
    # @validate_request_data

    def post(self, request, *args, **kwargs):
        pass
        # print("data:",request.data)
        #print("headers: ",request.headers)
        # program = Program.objects.create(
        #     company_name = request.data['title'],
        #     artist = request.data['artist']
        # )
        # return Response(
        #     data = SongsSerializer(a_song).data,
        #     status = status.HTTP_201_CREATED
        # )


class ProgramDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    GET program/:id/
    PUT program/:id/
    DELETE program/:id/
    """
    queryset = Program.objects.all()

    serializer_class = ProgramsSerializer
    permission_classes = (permissions.IsAuthenticated, IsStudent,)

    def get(self, request, *args, **kwargs):
        try:
            program = self.queryset.get(pk=kwargs["pk"])
            return Response(ProgramsSerializer(program).data)
        except Program.DoesNotExist:
            return Response(
                data={
                    "message": f"Program with id: {kwargs['pk']} not found"
                },
                status=status.HTTP_404_NOT_FOUND
            )

    # @validate_request_data
    # def put(self,request,*args,**kwargs):
    #     try:
    #         a_song = self.queryset.get(pk=kwargs["pk"])
    #         serializer = SongsSerializer()
    #         updated_song = serializer.update(a_song,request.data)
    #         return Response(SongsSerializer(updated_song).data)
    #     except Songs.DoesNotExist:
    #         return Response(
    #             data={
    #             "message": "Songs with id: {kwargs['pk']} not found"
    #             },
    #             status= status.HTTP_404_NOT_FOUND
    #         )

    # def delete(self,request,*args,**kwargs):
    #     try:
    #         a_song = self.queryset.get(pk=kwargs["pk"])
    #         a_song.delete()
    #         return Response(status=status.HTTP_204_NO_CONTENT)
    #     except Songs.DoesNotExist:
    #         return Response(
    #             data={
    #             "message": f"Songs with id: {kwargs['pk']} not found"
    #             },
    #             status= status.HTTP_404_NOT_FOUND
    #         )


#for managers
class ListCreateTakenProgramView(generics.ListCreateAPIView):
    """
    GET applications/
    POST applications/
    """
    model = TakenProgram

    def get_queryset(self):
        program = self.request.query_params.get('program_id')
        print(program)
        if program:
            return TakenProgram.objects.prefetch_related(
                Prefetch('student', queryset=Student.objects.only('pk').all()),
                Prefetch('program', queryset=Program.objects.only('pk').all())
            ).order_by('id').filter(program__posted_by=self.request.user.manager, program__id=program)
        return TakenProgram.objects.prefetch_related(
            Prefetch('student', queryset=Student.objects.only('pk').all()),
            Prefetch('program', queryset=Program.objects.only('pk').all())
        ).order_by('id').filter(program__posted_by=self.request.user.manager)

    serializer_class = TakenProgramSerializer
    permission_classes = (permissions.IsAuthenticated,IsManager)
    pagination_class = TakenProgramPagination
    # @validate_request_data

    def post(self, request, *args, **kwargs):
        pass
        # print("data:",request.data)
        #print("headers: ",request.headers)
        # program = Program.objects.create(
        #     company_name = request.data['title'],
        #     artist = request.data['artist']
        # )
        # return Response(
        #     data = SongsSerializer(a_song).data,
        #     status = status.HTTP_201_CREATED
        # )





class TakenProgramDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    GET application/:id/
    PUT application/:id/
    DELETE application/:id/
    """
    queryset = TakenProgram.objects.all()

    serializer_class = TakenProgramSerializer
    # permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            program = self.queryset.get(pk=kwargs["pk"])
            return Response(TakenProgramSerializer(program).data)
        except Program.DoesNotExist:
            return Response(
                data={
                    "message": f"Program with id: {kwargs['pk']} not found"
                },
                status=status.HTTP_404_NOT_FOUND
            )

    # @validate_request_data
    # def put(self,request,*args,**kwargs):
    #     try:
    #         a_song = self.queryset.get(pk=kwargs["pk"])
    #         serializer = SongsSerializer()
    #         updated_song = serializer.update(a_song,request.data)
    #         return Response(SongsSerializer(updated_song).data)
    #     except Songs.DoesNotExist:
    #         return Response(
    #             data={
    #             "message": "Songs with id: {kwargs['pk']} not found"
    #             },
    #             status= status.HTTP_404_NOT_FOUND
    #         )

    # def delete(self,request,*args,**kwargs):
    #     try:
    #         a_song = self.queryset.get(pk=kwargs["pk"])
    #         a_song.delete()
    #         return Response(status=status.HTTP_204_NO_CONTENT)
    #     except Songs.DoesNotExist:
    #         return Response(
    #             data={
    #             "message": f"Songs with id: {kwargs['pk']} not found"
    #             },
    #             status= status.HTTP_404_NOT_FOUND
    #         )


# class SongsDetailView(generics.RetrieveUpdateDestroyAPIView):
#     """
#     GET songs/:id/
#     PUT songs/:id/
#     DELETE songs/:id/
#     """
#     queryset = Songs.objects.all()
#     serializer_class = SongsSerializer()
#     permission_classes = (permissions.IsAuthenticated,)


#     def get(self,request,*args,**kwargs):
#         try:
#             a_song = self.queryset.get(pk=kwargs["pk"])
#             return Response(SongsSerializer(a_song).data)
#         except Songs.DoesNotExist:
#             return Response(
#                 data={
#                 "message": f"Songs with id: {kwargs['pk']} not found"
#                 },
#                 status= status.HTTP_404_NOT_FOUND
#             )

#     @validate_request_data
#     def put(self,request,*args,**kwargs):
#         try:
#             a_song = self.queryset.get(pk=kwargs["pk"])
#             serializer = SongsSerializer()
#             updated_song = serializer.update(a_song,request.data)
#             return Response(SongsSerializer(updated_song).data)
#         except Songs.DoesNotExist:
#             return Response(
#                 data={
#                 "message": "Songs with id: {kwargs['pk']} not found"
#                 },
#                 status= status.HTTP_404_NOT_FOUND
#             )

#     def delete(self,request,*args,**kwargs):
#         try:
#             a_song = self.queryset.get(pk=kwargs["pk"])
#             a_song.delete()
#             return Response(status=status.HTTP_204_NO_CONTENT)
#         except Songs.DoesNotExist:
#             return Response(
#                 data={
#                 "message": f"Songs with id: {kwargs['pk']} not found"
#                 },
#                 status= status.HTTP_404_NOT_FOUND
#             )


class ListCreateAppliedProgramView(generics.ListCreateAPIView):
    """
    GET applied/
    POST applied/
    """
    model = TakenProgram

    def get_queryset(self):
        program = self.request.query_params.get('program_id')
        print(program)
        if program:
            return TakenProgram.objects.prefetch_related(
                Prefetch('student', queryset=Student.objects.only('pk').all()),
                Prefetch('program', queryset=Program.objects.only('pk').all())
            ).order_by('id').filter(student=self.request.user.student, program__id=program)
        return TakenProgram.objects.prefetch_related(
            Prefetch('student', queryset=Student.objects.only('pk').all()),
            # Prefetch('program', queryset=Program.objects.only('pk').all())
        ).order_by('id').filter(student=self.request.user.student)

    serializer_class = TakenProgramSerializer
    permission_classes = (permissions.IsAuthenticated,IsStudent)
    pagination_class = TakenProgramPagination
    # @validate_request_data

    def post(self, request, *args, **kwargs):
        pass
        # print("data:",request.data)
        #print("headers: ",request.headers)
        # program = Program.objects.create(
        #     company_name = request.data['title'],
        #     artist = request.data['artist']
        # )
        # return Response(
        #     data = SongsSerializer(a_song).data,
        #     status = status.HTTP_201_CREATED
        # )




class LoginView(generics.CreateAPIView):
    """
    POST auth/login/
    """
    # This permission class will overide the global permission
    # class setting
    permission_classes = (permissions.AllowAny,)

    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        email = request.data.get("email", "")
        password = request.data.get("password", "")
        if not email or not password:
            return Response(
                data={
                    "message": "email, password is required to login"
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        user = authenticate(request, username=email, password=password)

        if user is not None:
            # login saves the user’s ID in the session,
            # using Django’s session framework.
            #login(request, user)
            serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )})
            serializer.is_valid()
            user_serializer = UserSerializer(user)
            return Response({
                'jwt': serializer.data,
                'credential': user_serializer.data
            }
            )

        return Response(
            data={
                "message": "User not authorised"
            },
            status=status.HTTP_401_UNAUTHORIZED
        )


class RegisterStudentView(generics.CreateAPIView):
    """
    POST auth/register/manager/
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        firstname = request.data.get("firstname", "")
        lastname = request.data.get("lastname", "")
        password = request.data.get("password", "")
        email = request.data.get("email", "")
        college = request.data.get("college", "")
        if college == "":
            college = 1
        if not all([firstname, lastname, password, email, college]):
            return Response(
                data={
                    "message": "All fields are required",
                    "format": {
                        "first_name": "First name (required)",
                        "last_name": "Last name (required)",
                        "password": "Desired password (required)",
                        "email": "Users email (required)",
                        "college": "Student's College (required)",

                    }
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        college = College.objects.get(pk=college)
        try:
            new_user = User.objects.create_user(
                first_name=firstname, last_name=lastname, username=email, password=password, email=email
            )
        except IntegrityError as e:
            print(e)
            return Response(
                data={
                    "message": str(e.__cause__).rstrip().partition("Key ")[2]

                },
                status=status.HTTP_400_BAD_REQUEST
            )
        age = 0
        phone = 0
        new_student = Student.objects.create(
            user=new_user,
            age=age,
            college=college,

        )
        new_user.is_student = True
        new_user.save()

        return Response(
            data=StudentSerializer(new_student).data,
            status=status.HTTP_201_CREATED
        )

        # user_created=False
        # student_created=False #failsafe
        # new_user = User.objects.create_user(
        #     username=username, password=password, email=email,is_active=True
        # )

        # user_created=True
        # college = College.objects.get(pk=college)
        # new_student = Student.objects.create(
        #     user=new_user,
        #     age=age,
        #     college=college,
        #     phone = phone,

        # )
        # new_user.is_student=True
        # student_created=True

        # except djongo.sql2mongo.SQLDecodeError:

        #     if user_created:
        #         new_user.delete()

        #     return Response(
        #         data = {
        #             "message": "User with that credentials already exists"

        #         },
        #         status = status.HTTP_400_BAD_REQUEST
        #     )

        # except Exception as e:

        #     if user_created:
        #         new_user.delete()

        #     return Response(
        #         data={
        #             "message": str(e)
        #         }
        #     )

        # return Response(
        #     data = StudentSerializer(new_student).data,
        #     status=status.HTTP_201_CREATED
        #     )


class RegisterManagerView(generics.CreateAPIView):
    """
    POST auth/register/manager/
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        first_name = request.data.get("firstname", "")
        email = request.data.get("email", "")
        password = request.data.get("password", "")
        company = request.data.get("company", "")
        phone = request.data.get("phone", "")
        if not all([first_name, password, email, company, phone]):
            return Response(
                data={
                    "message": "All fields are required",
                    "format": {
                        "firstname": "First name (required)",
                        "password": "Desired password (required)",
                        "email": "Users email (required)",
                        "company": "Manager's Company (required)",
                        "phone": "Manager's phone (required)"
                    }
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            new_user = User.objects.create_user(
                first_name=first_name, username=email, password=password, email=email
            )
        except IntegrityError as e:
            return Response(
                data={
                    "message": str(e.__cause__).rstrip().partition("DETAIL:  ")[2]

                },
                status=status.HTTP_400_BAD_REQUEST
            )
        new_manager = Manager.objects.create(
            user=new_user,
            company=company,
            phone=phone,

        )
        new_user.is_manager = True
        new_user.save()

        return Response(
            data=ManagerSerializer(new_manager).data,
            status=status.HTTP_201_CREATED
        )

        # user_created=False
        # student_created=False #failsafe
        # new_user = User.objects.create_user(
        #     username=username, password=password, email=email,is_active=True
        # )

        # user_created=True
        # college = College.objects.get(pk=college)
        # new_student = Student.objects.create(
        #     user=new_user,
        #     age=age,
        #     college=college,
        #     phone = phone,

        # )
        # new_user.is_student=True
        # student_created=True

        # except djongo.sql2mongo.SQLDecodeError:

        #     if user_created:
        #         new_user.delete()

        #     return Response(
        #         data = {
        #             "message": "User with that credentials already exists"

        #         },
        #         status = status.HTTP_400_BAD_REQUEST
        #     )

        # except Exception as e:

        #     if user_created:
        #         new_user.delete()

        #     return Response(
        #         data={
        #             "message": str(e)
        #         }
        #     )

        # return Response(
        #     data = StudentSerializer(new_student).data,
        #     status=status.HTTP_201_CREATED
        #     )


# class LogoutView(generics.ListCreateAPIView):
#     queryset = User.objects.all()
#     def get(self, request, *args,**kwargs):
#         # simply delete the token to force a login
#         if request.user.is_authenticated:
#             request.user.auth_token.delete()
#             return Response(
#                 data={
#                     "message":"Succesfully logged out"
#                 },
#                 status=status.HTTP_200_OK)
#         else:
#             return Response(
#                 data={
#                     "message":"User not logged in"
#                 },
#                 status=status.HTTP_400_BAD_REQUEST
#                 )


class ListDistrictView(generics.ListAPIView):
    """
    GET city/
    """
    queryset = District.objects.all()
    serializer_class = DistrictSerializer
    # permission_classes = (permissions.IsAuthenticated,)
    # filterset_class = StudentFilter


class ListCountryView(generics.ListAPIView):
    """
    GET country/
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class ListStateView(generics.ListAPIView):
    """
    GET state/
    """
    queryset = State.objects.all()
    serializer_class = StateSerializer
    filterset_class = StateFilter


class ListCategoryView(generics.ListAPIView):
    """
    GET category/
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ListCollegeView(generics.ListAPIView):
    """
    GET college/
    """
    queryset = College.objects.prefetch_related(
        Prefetch('district', queryset=State.objects.only('pk').all()),
        Prefetch('state', queryset=State.objects.only('pk').all())
    ).all()
    serializer_class = CollegeSerializer
    filterset_class = CollegeFilter


class CollegeDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    GET college/:id/
    PUT college/:id/
    DELETE college/:id/
    """
    queryset = College.objects.all()
    serializer_class = CollegeSerializer()
    # permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            college = self.queryset.get(pk=kwargs["pk"])
            return Response(CollegeSerializer(college).data)
        except College.DoesNotExist:
            return Response(
                data={
                    "message": f"College with id: {kwargs['pk']} not found"
                },
                status=status.HTTP_404_NOT_FOUND
            )

    # @validate_request_data
    # def put(self,request,*args,**kwargs):
    #     try:
    #         a_song = self.queryset.get(pk=kwargs["pk"])
    #         serializer = SongsSerializer()
    #         updated_song = serializer.update(a_song,request.data)
    #         return Response(SongsSerializer(updated_song).data)
    #     except Songs.DoesNotExist:
    #         return Response(
    #             data={
    #             "message": "Songs with id: {kwargs['pk']} not found"
    #             },
    #             status= status.HTTP_404_NOT_FOUND
    #         )

    # def delete(self,request,*args,**kwargs):
    #     try:
    #         a_song = self.queryset.get(pk=kwargs["pk"])
    #         a_song.delete()
    #         return Response(status=status.HTTP_204_NO_CONTENT)
    #     except Songs.DoesNotExist:
    #         return Response(
    #             data={
    #             "message": f"Songs with id: {kwargs['pk']} not found"
    #             },
    #             status= status.HTTP_404_NOT_FOUND
    #         )


def activate(request, uidb64, token):
    try:
        print(type(uidb64))
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)

    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_verified = True
        user.save()
        return JsonResponse(data={
            "message": 'Thank you for your email confirmation. Now you can login your account.'
        },
            status=status.HTTP_200_OK

        )
    else:
        return JsonResponse(data={
            "message": 'Account is already activated or activation link is invalid!'
        },
            status=status.HTTP_400_BAD_REQUEST
        )


class verify_email(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            user = request.user
            email = request.user.username
            current_site = get_current_site(request)
            mail_subject = 'Activate your Zeloband account.'
            user = request.user
            uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
            token = account_activation_token.make_token(user)
            ctx = {
                'user': user,
                'domain': current_site.domain,
                'uid': uid,
                'token': token,
            }

            message = get_template('activate_acc.html').render(ctx)
            to_email = email
            mail = EmailMessage(mail_subject, message, to=[to_email])
            mail.content_subtype = 'html'
            mail.send()
            return Response(
                data={
                    "message": UserSerializer(user).data
                },
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return Response(
                data={
                    "message": str(e)
                },
                status=status.HTTP_501_NOT_IMPLEMENTED
            )


class ListNotificationView(generics.ListAPIView):
    """
    GET students/
    """
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return Notification.objects.filter(
            recipient=self.request.user.pk).order_by('-timestamp')


def main_site(request):
    return redirect("http://localhost:3000/")


@login_required
def clearSession(request):
    logout(request)
    return JsonResponse(data={
        "message": 'Logged out'
    },
        status=status.HTTP_200_OK
    )
