print('to run: ./manage.py shell < upload_csv.py')
from backend.models import State,City
import csv
import os

path =  './db_scripts/cities.csv'

f = open(path)
length = len(f.readlines())
with open(path) as f:
    reader = csv.reader(f)
    #next(reader)
    #length = len(list(reader))
    for row_number,row in enumerate(reader):
        #print(row[3].capitalize())
        state = State.objects.get(name=row[3].capitalize())
        _,created = City.objects.get_or_create(state=state,name=row[1].capitalize())
        #print(state)
        read_percentage = round(100*(row_number/length),1)
        print(f'{read_percentage}% inserted',end="\r")
print()
print("done!")