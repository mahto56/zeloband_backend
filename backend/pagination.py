from rest_framework import pagination


class ProgramPagination(pagination.PageNumberPagination):
    page_size = 8


class TakenProgramPagination(pagination.PageNumberPagination):
    page_size = 8
