from django.contrib import admin
from django.apps import apps
from django.contrib.admin.sites import AlreadyRegistered
from .models import *
# Register your models here.
app_models = apps.get_app_config('backend').get_models()


class CollegeAdmin(admin.ModelAdmin):
    list_display = ('college_name', 'university_name', 'district',
                    'state', 'college_type', 'no_of_students', 'description')
    readonly_fields = ('state', 'country')
    list_filter = ('state', 'district')
    # search_fields = ('state__name',)
    sortable_by = list_filter


class ProgramAdmin(admin.ModelAdmin):
    list_display = ('program_name', 'posted_by', 'application_deadline', 'status',
                    'posted_on')
    list_editable = ('status',)
    list_filter = ('posted_by', 'status')
    search_fields = ('program_name', 'posted_by__user__username')
    list_per_page = 5


class ManagerAdmin(admin.ModelAdmin):
    list_display = ('user__username', 'user__first_name', 'user__last_name', 'phone', 'company',
                    'user__last_login', 'user__date_joined',)

    def user__username(self, obj):
        return obj.user.username

    def user__first_name(self, obj):
        return obj.user.first_name

    def user__last_name(self, obj):
        return obj.user.last_name

    def user__last_login(self, obj):
        return obj.user.last_login

    def user__date_joined(self, obj):
        return obj.user.date_joined

    # list_editable = ('user__is_active',)

    # def user__is_active(self, obj):
    #     return obj.user.is_active

    list_filter = ('user__username', 'user__first_name',
                   'user__date_joined', 'user__last_login',)
    search_fields = ('user__first_name', 'user__username')
    list_per_page = 5


admin.site.site_header = 'Zeloband Administration'
# admin.site.site_title = 'Zeloband Administration'

admin.site.register(College, CollegeAdmin)
admin.site.register(Program, ProgramAdmin)
admin.site.register(Manager, ManagerAdmin)


for model in app_models:
    try:
        admin.site.register(model)
    except AlreadyRegistered:
        pass
